/**
 * Vuex module for work with API
 */

import {AxiosError} from "axios";
import {GetterTree, MutationTree, ActionTree} from "vuex";
// import moment from "moment";
import {IStateIndex} from "./index";
import {APIClient} from "~/libs/api/client";
import {APIConfig, APIRequest, APIFullResponse, APIResponse} from "~/libs/api/types";

/* Auth token header name if not specified in config */
const DEFAULT_AUTH_TOKEN_HEADER: string = "X-Auth-Token";
// TODO X-API-Version header
/* API version header name if not specified in config */
// const DEFAULT_API_VERSION_HEADER: string = "X-API-Version";

export interface IStateAPI {
    client: APIClient, // API client (copy of context.$api)
    config: APIConfig, // config of API client
}

export const state = (): Partial<IStateAPI> => ({
    client: undefined,
    config: undefined,
});

export const getters: GetterTree<IStateAPI, IStateIndex> = {
};

export const mutations: MutationTree<IStateAPI> = {
    init(state: IStateAPI, {client, config}): void {
        state.client = client;
        state.config = config;
    },
};

export const actions: ActionTree<IStateAPI, IStateIndex> = {
    /**
     * Initializes API, creates client and sets listeners
     */
    async init({commit, dispatch}, config: APIConfig): Promise<APIClient> {
        config.prepareRequest = async (request: APIRequest): Promise<any> => {
            return dispatch("prepareRequest", request);
        };
        config.parseResponse = async (response: APIFullResponse): Promise<any> => {
            return dispatch("parseResponse", response);
        };
        config.block = async (block: boolean): Promise<any> => {
            return dispatch("block", block);
        };
        config.error = async (e: Error): Promise<any> => {
            return dispatch("onPageError", e);
        };
        if (!config.authTokenHeader) {
            config.authTokenHeader = DEFAULT_AUTH_TOKEN_HEADER;
        }

        // TODO X-API-Version header
        // if (!config.apiVersionHeader) {
        //     config.apiVersionHeader = DEFAULT_API_VERSION_HEADER;
        // }
        const client: APIClient = new APIClient(config);
        commit("init", {client, config});
        return client;
    },

    /**
     * Prepares "short" request to "full"
     */
    async prepareRequest({state, rootGetters}, request: APIRequest): Promise<APIRequest> {
        request = Object.assign({}, request);
        if (request.data) {
            request.data = {
                data: request.data,
            };
        }
        if (!request.headers) {
            request.headers = {};
        }
        const token: string|undefined = rootGetters["auth/token"];
        if (token) {
            request.headers[state.config.authTokenHeader || DEFAULT_AUTH_TOKEN_HEADER] = token;
            // TODO X-API-Version header
            // request.headers[state.config.apiVersionHeader || DEFAULT_API_VERSION_HEADER] = moment().format("YYYYMMDD");
        }
        return request;
    },

    /**
     * Parses "full" response, handles root data, coverts it to "short" response
     */
    async parseResponse({dispatch}, response: APIFullResponse): Promise<APIResponse> {
        response = Object.assign({}, response);
        if (response.access_token) {
            await dispatch("auth/setToken", response.access_token, {root: true});
        }
        return response.data || {};
    },

    /**
     * Handles block requests
     */
    async block({dispatch}, isBlocked: boolean): Promise<void> {
        const action: string = isBlocked ? "block" : "release";
        await dispatch(`ui/${action}`, null, {root: true});
    },

    /**
     * Handles error HTTP response
     */
    async onPageError({dispatch}, e: AxiosError): Promise<void> {
        const code: number = (e.response && e.response.status) || 500;
        await dispatch("ui/errorPage", code, {root: true});
    },

    /**
     * Alias of context.$api.request()
     */
    async request({state}, request: APIRequest): Promise<APIResponse> {
        console.log("API request: ", request);
        return state.client.request(request);
    },
};
