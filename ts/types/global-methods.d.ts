import Vue from "vue";

declare module "vue/types/vue" {
    interface Vue {
        $getAge(stringProp: string): string,

        $resourceViewType(identifier: string): number,
    }
}
