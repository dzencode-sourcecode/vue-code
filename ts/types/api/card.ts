import {ItemFieldValues} from "./fields";
import {RelationsList} from "./relations";
import {ResourceSchema} from "~/types/api/resources";

/**
 * Данные для страницы карточки
 */
export interface ResourceCard {
    type: string; // идентификатор типа ресурса (например, "person")
    id: number; // id объекта
    fields: ItemFieldValues; // значения полей
    relations?: RelationsList; // список релейшенов, если реализует данный интерфейс
    // @todo ...
}

/**
 * Ответ на запрос карточки
 */
export interface ResourceCardResponse {
    card: ResourceCard;
    schema?: ResourceSchema; // схема ресурса, если была запрошена
}
