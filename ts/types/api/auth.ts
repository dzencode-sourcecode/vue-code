import {ModulesList} from "~/types/api/modules";

/**
 * Данные текущей сессии
 */
export interface UserSession {
    user: {
        name: string;
        locale: string; // "en", "nl" ...
        company: {
            name: string;
        };
    };
    /* Список доступных модулей, необходимы всегда для отрисовки меню и роутинга */
    modules: ModulesList;
}

export interface LoginRequest {
    username: string;
    password: string;
    tenant: string;
}

export interface LoginResponse {
    session: UserSession;
}

export interface SessionResponse {
    session: UserSession;
}
