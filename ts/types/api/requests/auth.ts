/**
 * Запросы, связанные с авторизацией
 */

import {LoginRequest, LoginResponse, SessionResponse} from "../auth";

/**
 * Форма логина.
 */
interface APIAuthLogin {
    action: "auth/login",
    method: "POST",
    request: LoginRequest,
    response: LoginResponse,
    errors: {
        403: "user with specified login/password is not found",
    },
    rootResponse: {
        // При логине фронтенд получает авторизационный-токен, который сохраняет у себя в хранилище.
        // Можно было возвращать его в response.
        // Однако, уже есть функционал по сохранению токена, приходящего в root-данных (../request.ts)
        // так что используем её
        token: string;
    },
}

/**
 * Логаут
 */
interface APIAuthLogout {
    action: "auth/logout",
    method: "POST",
    request: {},
    response: {}, // если ответ 200, то и так значит, что всё прошло успешно
    rootResponse: {
        token: false; // сброс токена
    },
}

/**
 * Запрос сессии.
 * На случай перезагрузки страницы (страница загрузилась, данных нет, но в storage лежит токен).
 */
interface APIAuthSession {
    action: "auth/session",
    method: "GET",
    response: SessionResponse,
    errors: {
        401: "token is not found or expired",
    },
    rootResponse: {
        token: false; // сброс токена, если он не валидный
    },
}
