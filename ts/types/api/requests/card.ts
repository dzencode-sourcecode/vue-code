/**
 * API-интерфейс: ресурс имеет страницу-карточку установленного образца
 */

import {ResourceCardResponse} from "../card";

/**
 * Запрос карточки
 */
interface APIResourceSchema {
    action: "{identifier}/{id}", // например, `/api/person/1234`
    method: "GET",
    get: {
        schema: boolean; // GET-параметр, указывающий на то, что следует к ответу приаттачить схему ресурса
    },
    response: ResourceCardResponse;
    errors: {
        404: "not found",
    },
}
