/**
 * Typing after inject API (see ~/plugins/api.ts)
 */
import {APIClient} from "~/libs/api/client";

declare module "vue/types/vue" {
    interface Vue {
        $api: APIClient;
    }
}

declare module "@nuxt/types" {
    interface Context {
        $api: APIClient;
    }
    interface NuxtAppOptions {
        $api: APIClient;
    }
}

declare module "vuex/types/index" {
    interface Store<S> {
        $api: APIClient;
    }
}
