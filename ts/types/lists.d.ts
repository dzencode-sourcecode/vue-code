import {IFieldType} from "~/types/fields";
import {IResourcePermission} from "~/types/resources";

export interface IResourceList {
    columns: Array<IColumn>;
    columnsCustom?: string[];
}

export interface IColumn {
    identifier: string;
    field?: string;
    sortable: boolean;
}

export interface ITableHeaders {
    text: string;
    value: string;
    align?: "start" | "center" | "end";
    sortable?: boolean;
    filterable?: boolean;
    groupable?: boolean;
    divider?: boolean;
    class?: string | string[];
    width?: string | number;
    type: string;
    options?: Array<any>;
    showColumn?: boolean;
    filter?: (value: any, search: string, item: any) => boolean;
    sort?: (a: any, b: any) => number;
}

export interface ITableRowData {
    id: number;
    fields: {
        [identifier: string]: string | number;
    };
    permissions?: Partial<IResourcePermission>;
}

export interface ITableData {
    id?: number;
    resources: Array<ITableRowData>;
    count: number;
    type?: IFieldType; // TODO: check type
}

export interface ITableOptions {
    page?: number;
    itemsPerPage?: number;
    sortBy?: string[];
    sortDesc?: boolean[];
    groupBy?: string[];
    groupDesc?: boolean[];
    multiSort?: boolean;
    mustSort?: boolean;
}
