import Vue from 'vue'
import Vuex from 'vuex'

import apiModule from './api'
import userModule from './user'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        api: apiModule,
        user: userModule,
    },
    state: {
        users: [],
        notifications: [],
        planningConfig: {},
        countOnlineUsers: 0,
        DATETIME_FOMRAT: 'YYYY-MM-DDTHH:mm:ssZ',
        TIME_SOURCE: { manual: 2, tracked: 3 },
        drawer: false,
        dialog: false,
        confirm: false,
        loader: false,
        userConfigs: [],
    },
    mutations: {
        setUsers(state, users) {
            state.users = users
        },
        setUserNotification(state, notifications) {
            console.log('setUserNotification: ', notifications)
            state.notifications = notifications
        },
        setMyPlanningConfig(state, config) {
            state.planningConfig = config
        },
        setOnlineUsers(state, onlineUsers) {
            state.countOnlineUsers = 0
            state.users.map(user => {
                const onlineUser = onlineUsers.find(
                    element => element.userId === user.id
                )
                if (onlineUser) {
                    user.statusOnline = true
                    user.appVersion = onlineUser.appVersion
                    user.status = onlineUser.status
                    state.countOnlineUsers++
                } else {
                    user.statusOnline = false
                    user.appVersion = null
                    user.status = null
                }
            })
            state.users.sort((a, b) => {
                return a.statusOnline === b.statusOnline ? 0 : a.statusOnline ? -1 : 1
            })
        },
        setUserConfigs: (state, payload) => (state.userConfigs = payload),
        setDrawer: (state, payload) => (state.drawer = payload),
        toggleDrawer: state => (state.drawer = !state.drawer),
    },
    actions: {
        getAllUsers({ commit, dispatch }) {
            dispatch('api/apiEmployeesList')
                .then(resp => {
                    commit('setUsers', resp.data.results)
                })
                .catch(err => console.log('error while dispatching action: ', err))
        },
        getOnlineUsers({ commit, dispatch }) {
            dispatch('api/apiOnlineEmployees').then(resp => {
                commit('setOnlineUsers', resp)
            })
        },
    },
    getters: {
        userById: state => id => {
            id = Number(id)
            return state.users.find(user => user.id === id)
        },
        userByName: state => name => {
            return state.users.filter(
                user =>
                    (user.last_name || '').toLowerCase().indexOf(name.toLowerCase()) !==
                    -1 ||
                    (user.name || '').toLowerCase().indexOf(name.toLowerCase()) !== -1
            )
        },
        getNotificationsByCreator: state => id => {
            return state.notifications.filter(notification => (notification.userCreatorId === id))
        },
        getNotificationsByResponsible: state => id => {
            return state.notifications.filter(notification => (notification.userResponsibleId === id))
        },
    },
})
