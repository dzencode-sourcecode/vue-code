import axios from 'axios'
import moment from 'moment'
import { API_URLS, TRACKER_URLS, DASHBOARD_URLS } from '@/const'

const API_HOST = process.env.VUE_APP_API_HOST
const TRACKER_API_HOST = process.env.VUE_APP_TRACKER_API_HOST
const DASHBOARD_API_HOST = process.env.VUE_APP_DASHBOARD_API_HOST
const TRACKER_TOKEN =
    'Bearer for test'
const DASHBOARD_TOKEN =
    'Bearer for test'
const API_TOKEN = 'Bearer for test'

export default {
    namespaced: true,
    state: {
        trackerApiHost: TRACKER_API_HOST,
        dashboardApiHost: DASHBOARD_API_HOST,
        axios: axios.create({
            baseURL: API_HOST,
            headers: { Authorization: API_TOKEN },
        }),
        axiosTracker: axios.create({
            baseURL: TRACKER_API_HOST,
            headers: { Authorization: TRACKER_TOKEN },
        }),
        axiosDashboard: axios.create({
            baseURL: DASHBOARD_API_HOST,
            headers: { Authorization: DASHBOARD_TOKEN },
        }),
        axiosMultipartDashboard: axios.create({
            baseURL: DASHBOARD_API_HOST,
            headers: {
                Authorization: DASHBOARD_TOKEN,
                'Content-Type': 'multipart/form-data',
            },
        }),
    },
    actions: {
        apiTimeTracks({ state }, params) {
            params = new URLSearchParams(params)
            return state.axios.get(API_URLS.timecosts, { params })
        },
        apiTasksWithStats({ state }, params) {
            params = new URLSearchParams(params)
            return state.axios.get(API_URLS.tasksWithStats, { params })
        },
        async apiTasks({ state }, params) {
            params = new URLSearchParams(params)
            return state.axios.get(API_URLS.tasks, { params })
        },
        async apiProjects({ state }, params) {
            params = new URLSearchParams(params)
            return state.axios.get(API_URLS.projects, { params })
        },
        apiProject({ state }, id) {
            return state.axios.get(API_URLS.projects + id + '/')
        },
        apiDeals({ state }) {
            return state.axios.get(API_URLS.deals)
        },
        apiEmployeesList({ state }) {
            return state.axios.get(API_URLS.employees)
        },
        apiScreenshots({ state }, { userId, params }) {
            // params: startDate, stopDate
            return state.axiosTracker
                .get(TRACKER_URLS.screenshots + userId + '/', { params })
                .then(resp => {
                    const screens = resp.data.data
                    screens.forEach(screen => {
                        screen.date = moment(screen.createdDate)
                        screen.links = screen.photos.map(
                            photo => state.trackerApiHost + photo.link
                        )
                    })
                    return screens
                })
        },
        apiActivity({ state }, { userId, params }) {
            return state.axiosTracker
                .get(TRACKER_URLS.activity + userId + '/', { params })
                .then(resp => {
                    const activity = resp.data
                    activity.map(item => {
                        item.date = moment(item.createdDate)
                        item.data.forEach(action => {
                            item[action.action] = action.count
                        })
                    })
                    return activity
                })
        },
    },
    mutations: {},
    getters: {},
}
