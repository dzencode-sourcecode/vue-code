import ApiService from "@/common/api.service";
import {
    ACTIVE_ACCOUNT,
    AUTH_TOKEN_KEY,
    BASE_DOMAIN,
    SOCIAL_AUTH_HOST,
} from "@/common/config";
import { updateFavicon } from "@/assets/js/updateFavion.js";
import { updateTitle } from "@/assets/js/updateTitle";

// action types
export const VERIFY_AUTH = "verifyAuth";
export const LOGIN = "login";
export const GET_USER = "getUser";
export const AUTH_WITH_SOCIAL = "authWithSocial";
export const SOCIAL_LOGIN_REDIRECT = "socialLoginRedirect";
export const SOCIAL_LOGIN_CALLBACK = "socialLoginCallback";
export const SOCIAL_ATTACH_REDIRECT = "socialAttachCallback";
export const SOCIAL_ATTACH_CALLBACK = "socialRedirectCallback";
export const DISCONNECT_SOCIAL = "disconnectSocial";
export const LOGOUT = "logout";
export const REGISTER = "register";
export const UPDATE_USER = "updateUser";
export const UPDATE_SOCIALS = "updateSocials";
export const GET_NOTIFICATIONS = "getNotifications";
export const UPDATE_NOTIFICATIONS = "updateNotifications";
export const GET_CURRENT_PROJECTS = "getCurrentProjects";
export const GET_CURRENT_TARIFF = "getCurrentTariff";
export const GET_MY_PERMISSIONS = "getMyPermissions";
export const GET_MY_PROJECTS = "getMyProjects";
export const CONFIRM_EMAIL = "confirmEmail";
export const GET_SOCIAL_AUTH_URL = "getSocialAuthUrl";
export const GET_PROJECT_STAFF = "getProjectStaff";

// mutation types
export const PURGE_AUTH = "logOut";
export const SET_AUTH = "setAuth";
export const SET_USER = "setUser";
export const SET_SOCIALS = "setSocials";
export const SET_ERROR = "setError";
export const SET_NOTIFICATIONS = "setNotifications";
export const SET_ACTIVE_ACCOUNT = "setActiveAccount";
export const SET_CURRENT_PROJECTS = "setCurrentProjects";
export const SET_CURRENT_TARIFF = "setCurrentTariff";
export const SET_MY_PERMISSIONS = "setMyPermissions";
export const SET_AUTH_FLAG = "setAuthFlag";
export const SET_AVATAR = "setAvatar";
export const SET_PROJECT_STAFFS = "setProjectStaff";

const state = {
    errors: {},
    user: {},
    isAuthenticated: false,
    activeAccount: 0,
    currentProjects: [],
    currentTariff: {},
    myPermissions: [],
    staff: [],
};

// **** GETTERS **** //
const getters = {
    getStaff(state) {
        return state.staff;
    },
    currentUser(state) {
        return state.user;
    },
    isProfileLoaded(state) {
        return state.user?.id;
    },
    profileFullName(state, getters) {
        if (!getters.isProfileLoaded) {
            return "";
        }
        return state.user.first_name + " " + state.user.last_name;
    },
    profileInitials(state, getters) {
        if (!getters.isProfileLoaded) {
            return "";
        }
        return state.user.first_name.charAt(0).toUpperCase();
    },
    profilePhoto(state, getters) {
        if (!getters.isProfileLoaded) {
            return "";
        }
        return state.user.avatar;
    },
    isAuthenticated(state) {
        return state.isAuthenticated;
    },
    idAccount(state) {
        return +state.activeAccount || +localStorage.getItem(ACTIVE_ACCOUNT + state.user.id);
    },
    currentProject(state) {
        return state.currentProjects.find((project) => +project.id === +state.activeAccount);
    },
    currentTariff(state) {
        return state.currentTariff;
    },
    hasPermission: (state) => (permission) => {
        if (state.myPermissions === "all") return true;
        else return state.myPermissions.find((item) => item === permission);
    },
    staffById: (state) => (id) => {
        return state.staff.find((staff) => {
            return +staff.user.id === +id;
        });
    },
}; // getters

// **** ACTIONS **** //
const actions = {
    [LOGIN](context, { email, password, remember }) {
        return new Promise((resolve, reject) => {
            ApiService.post("auth/token/", { username: email, password })
                .then((response) => {
                    context.commit(SET_AUTH, { token: response.token, remember });
                    resolve(response);
                })
                .catch((data) => {
                    reject(data);
                });
        });
    },
    [LOGOUT](context) {
        context.commit(PURGE_AUTH);
    },
    [REGISTER](context, credentials) {
        return new Promise((resolve, reject) => {
            ApiService.post("auth/register/", credentials)
                .then((response) => {
                    context.commit(SET_AUTH, response);
                    resolve(response);
                })
                .catch((response) => {
                    reject(response);
                });
        });
    },
    [VERIFY_AUTH](context) {
        if (context.state.isAuthenticated) return true;

        const authToken = localStorage.getItem(AUTH_TOKEN_KEY);
        if (authToken) {
            ApiService.setAuthHeader(authToken);
            return new Promise((resolve) => {
                ApiService.get("auth/me")
                    .then((response) => {
                        context.commit(SET_AUTH_FLAG, true);
                        context.commit(SET_AUTH);
                        context.commit(SET_USER, response);
                        context.commit(SET_ACTIVE_ACCOUNT, context.getters.idAccount);
                        context.commit(SET_CURRENT_PROJECTS, response.projects);
                        context.dispatch(GET_MY_PERMISSIONS);
                        context.dispatch(GET_CURRENT_TARIFF);
                        resolve(true);
                    })
                    .catch((response) => {
                        if (response.user === false) {
                            context.commit(PURGE_AUTH);
                        } else {
                            context.commit(SET_ERROR, ["Some network error"]);
                        }
                        resolve(false);
                    });
            });
        }

        return false;
    },
    [AUTH_WITH_SOCIAL](context, { provider, token }) {
        let url = `${SOCIAL_AUTH_HOST}/oauth/${provider}`;
        if (token) url += "/" + token;
        const width = 750;
        const height = 450;
        const left = screen.width / 2 - width / 2;
        const top = screen.height / 2 - height / 2;
        const popup = window.open(
            url,
            "Подключить " + provider,
            `width=${width},height=${height},top=${top},left=${left},toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0`
        );
        popup.focus();
    },
    [GET_USER](context) {
        ApiService.get("auth/me").then((response) => {
            context.commit(SET_USER, response);
            const id = context.getters.idAccount;
            context.commit(SET_ACTIVE_ACCOUNT, id);
        });
    },
    [GET_CURRENT_TARIFF](context) {
        if (context.getters.currentProject) {
            ApiService.get("/billing/tariff/current").then((response) => {
                context.commit(SET_CURRENT_TARIFF, response);
                context.dispatch(GET_MY_PROJECTS);
            });
        }
    },
    [GET_CURRENT_PROJECTS](context) {
        if (context.getters.currentProject) {
            ApiService.get("/billing/tariff/current").then((response) => {
                context.commit(SET_CURRENT_TARIFF, response);
            });
        }
    },
    [GET_MY_PROJECTS](context) {
        if (context.getters.currentProject) {
            ApiService.get("/account/project/my_projects/").then((response) => {
                context.commit(SET_CURRENT_PROJECTS, response);
            });
        }
    },
    [UPDATE_USER](context, user) {
        return new Promise((resolve, reject) => {
            ApiService.patch("auth/me/", user)
                .then((response) => {
                    context.dispatch(GET_MY_PERMISSIONS);
                    context.commit(SET_USER, response);
                    resolve(response);
                })
                .catch((response) => {
                    reject(response);
                });
        });
    },
    [GET_NOTIFICATIONS](context) {
        return ApiService.get("/auth/me/notifications")
            .then((response) => {
                context.commit(SET_NOTIFICATIONS, response);
                return response;
            })
            .catch((response) => {
                return Promise.reject(response);
            });
    },
    [UPDATE_NOTIFICATIONS](context, notifications) {
        return ApiService.patch(`/auth/me/notifications/${notifications.id}/`, notifications)
            .then((response) => {
                context.commit(SET_NOTIFICATIONS, response);
                return response;
            })
            .catch((response) => {
                return Promise.reject(response);
            });
    },
    [SOCIAL_LOGIN_REDIRECT](context, provider) {
        // DEPRECATED
        return new Promise((resolve) => {
            ApiService.get(`oauth/${provider}/redirect`)
                .then((response) => resolve(response.data.url))
                .catch((response) => {
                    context.commit(SET_ERROR, response.errors);
                });
        });
    },
    [SOCIAL_LOGIN_CALLBACK](context, { code, provider, redirect_uri }) {
        return new Promise((resolve, reject) => {
            const token = localStorage.getItem(AUTH_TOKEN_KEY);
            localStorage.removeItem(AUTH_TOKEN_KEY);
            if (token) ApiService.setAuthHeader(token);
            ApiService.post("auth/social/token/", { code, provider, redirect_uri })
                .then((resp) => {
                    context.commit(SET_AUTH, resp);
                    resolve(resp);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
    [UPDATE_SOCIALS](context) {
        return ApiService.get("/auth/me/social_networks/")
            .then((response) => {
                context.commit(SET_SOCIALS, response);
                return response.data;
            })
            .catch((response) => {
                return Promise.reject(response);
            });
    },
    [GET_MY_PERMISSIONS](context) {
        if (context.getters.currentProject) {
            return ApiService.get("/account/project/permissions/my/")
                .then((response) => {
                    context.commit(SET_MY_PERMISSIONS, response);
                    return response;
                })
                .catch((response) => {
                    return response;
                });
        } else {
            return false;
        }
    },
    [CONFIRM_EMAIL]() {
        return ApiService.post("/auth/send-confirm-mail/")
            .then((response) => {
                return response;
            })
            .catch((response) => {
                return Promise.reject(response);
            });
    },
    [GET_SOCIAL_AUTH_URL](context, provider) {
        return ApiService.get("/auth/social/url/", { provider }).then((resp) => {
            return resp.url;
        });
    },
    [DISCONNECT_SOCIAL](context, provider) {
        return ApiService.post("/auth/social/disconnect/", { provider });
    },
    [GET_PROJECT_STAFF](context) {
        return new Promise((resolve) => {
            ApiService.get("/account/project/staff")
                .then((response) => {
                    context.commit(SET_PROJECT_STAFFS, response);
                    resolve(response);
                })
                .catch(() => {
                    resolve([]);
                });
        });
    },
}; // actions

// **** MUTATIONS **** //
const mutations = {
    [SET_ERROR](state, error) {
        state.errors = error;
    },
    [SET_AUTH](state, payload) {
        state.errors = {};
        if (payload && payload.token) {
            ApiService.setAuthHeader(payload.token);
            localStorage.setItem(AUTH_TOKEN_KEY, payload.token);
        }
    },
    [SET_USER](state, user) {
        state.user = user;
    },
    [SET_NOTIFICATIONS](state, notifications) {
        state.user.notifications = notifications;
    },
    [SET_SOCIALS](state, socials) {
        state.user.social_networks = socials;
    },
    [PURGE_AUTH](state) {
        state.isAuthenticated = false;
        state.user = {};
        state.errors = {};
        state.activeAccount = 0;
        state.currentProjects = [];
        state.currentTariff = {};
        state.myPermissions = [];
        ApiService.removeAuthHeader();
        localStorage.removeItem(AUTH_TOKEN_KEY);
    },
    [SET_ACTIVE_ACCOUNT](state, id) {
        if (id) {
            state.activeAccount = id;
            localStorage.setItem(ACTIVE_ACCOUNT + state.user.id, id);
            if (location.host === BASE_DOMAIN) {
                ApiService.setHeaderAccount(id);
            }
        }
    },
    [SET_CURRENT_PROJECTS](state, projects) {
        state.currentProjects = projects;
        const currentProject = getters.currentProject(state);
        if (currentProject && currentProject.favicon) {
            updateFavicon(currentProject.favicon);
            updateTitle(currentProject.name);
        }
    },
    [SET_CURRENT_TARIFF](state, tariff) {
        state.currentTariff = tariff;
    },
    [SET_MY_PERMISSIONS](state, permissions) {
        state.myPermissions = permissions;
    },
    [SET_AUTH_FLAG](state, value) {
        state.isAuthenticated = value;
    },
    [SET_AVATAR](state, value) {
        state.user.avatar = value;
    },
    [SET_PROJECT_STAFFS](state, value) {
        state.staff = value;
    },
}; // mutations

export default {
    state,
    actions,
    mutations,
    getters,
};
