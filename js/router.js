import Vue from "vue";
import Router from "vue-router";
import store from "@/store";
import { SET_ACTIVE_ACCOUNT, VERIFY_AUTH } from "@/store/auth.module";
import {
    AUTH_TOKEN_KEY,
    BASE_DOMAIN,
    BASE_HOST,
    HTTP_PROTOCOL,
    REDIRECT_URL_KEY,
} from "@/common/config";

Vue.use(Router);

const router = new Router({
    mode: "history",
    linkActiveClass: "kt-widget__item--active",
    routes: [
        {
            path: "/",
            redirect: "/dashboard",
            component: () => import("@/views/theme/Base"),
            meta: { requiresAuth: true },
            children: [
                {
                    path: "/dashboard",
                    name: "dashboard",
                    component: () => import(/* webpackChunkName: "dashboard" */ "@/views/pages/Dashboard"),
                },
                {
                    path: "/me",
                    component: () => import(/* webpackChunkName: "profile" */ "@/views/pages/profile/Index"),
                    children: [
                        {
                            path: "account",
                            name: "me-account",
                            component: () =>
                                import(
                                    /* webpackChunkName: "profile" */ "@/views/pages/profile/components/Account"
                                    ),
                        },

                        {
                            path: "profile",
                            name: "me-profile",
                            component: () =>
                                import(
                                    /* webpackChunkName: "profile" */ "@/views/pages/profile/components/Profile"
                                    ),
                        },

                        {
                            path: "staff",
                            name: "me-staff",
                            component: () =>
                                import(/* webpackChunkName: "profile" */ "@/views/pages/profile/components/Staff"),
                        },

                        {
                            path: "finance",
                            name: "me-finance",
                            component: () =>
                                import(
                                    /* webpackChunkName: "profile" */ "@/views/pages/profile/components/Finance"
                                    ),
                        },

                        {
                            path: "change-password",
                            name: "me-change-password",
                            component: () =>
                                import(
                                    /* webpackChunkName: "profile" */ "@/views/pages/profile/components/ChangePassword"
                                    ),
                        },

                        {
                            path: "notification-settings",
                            name: "me-notification-settings",
                            component: () =>
                                import(
                                    /* webpackChunkName: "profile" */ "@/views/pages/profile/components/NotificationSettings"
                                    ),
                        },

                        {
                            path: "affiliate-program",
                            name: "me-affiliate-program",
                            component: () =>
                                import(
                                    /* webpackChunkName: "profile" */ "@/views/pages/profile/components/AffiliateProgram"
                                    ),
                        },

                        {
                            path: "accounts",
                            name: "accounts",
                            component: () =>
                                import(/* webpackChunkName: "profile" */ "@/views/pages/accounts/index"),
                        },
                    ],
                },
                {
                    path: "/settings",
                    component: () =>
                        import(/* webpackChunkName: "settings" */ "@/views/pages/global-settings/Index"),
                    children: [
                        {
                            path: "staff",
                            name: "staff",
                            component: () =>
                                import(/* webpackChunkName: "settings" */ "@/views/pages/staff/StaffList"),
                        },
                        {
                            path: "history",
                            name: "history",
                            component: () =>
                                import(/* webpackChunkName: "settings" */ "@/views/pages/history/History"),
                        },
                        {
                            path: "global",
                            name: "global",
                            component: () =>
                                import(
                                    /* webpackChunkName: "settings" */ "@/views/pages/global-settings/components/Settings"
                                    ),
                        },
                        {
                            path: "payment-history",
                            name: "payment-history",
                            component: () =>
                                import(
                                    /* webpackChunkName: "settings" */ "@/views/pages/global-settings/components/PaymentHistory"
                                    ),
                        },
                    ],
                },
                {
                    path: "integrations",
                    name: "integrations",
                    component: () =>
                        import(/* webpackChunkName: "integrations" */ "@/views/pages/integrations/Index"),
                },
                {
                    path: "tariff",
                    name: "tariff",
                    component: () => import(/* webpackChunkName: "tariff" */ "@/views/pages/tariff/Index"),
                },
                {
                    path: "/staff/:id",
                    name: "staff-settings",
                    component: () =>
                        import(/* webpackChunkName: "users" */ "@/views/pages/staff/components/Side"),
                    children: [
                        {
                            path: "",
                            name: "staff-data",
                            component: () =>
                                import(/* webpackChunkName: "users" */ "@/views/pages/staff/StaffCreateDataLayout"),
                        },
                        {
                            path: "staff-permission",
                            name: "staff-permission",
                            component: () =>
                                import(
                                    /* webpackChunkName: "users" */ "@/views/pages/staff/components/StaffPermission"
                                    ),
                        },
                        {
                            path: "staff-control",
                            name: "staff-control",
                            component: () =>
                                import(
                                    /* webpackChunkName: "users" */ "@/views/pages/staff/components/StaffControl"
                                    ),
                        },
                    ],
                },
            ],
        }, // app layout
        {
            path: "/error",
            name: "error",
            component: () => import(/* webpackChunkName: "error" */ "@/views/pages/error/Error"),
            children: [
                {
                    path: "error-1",
                    name: "error-1",
                    component: () => import(/* webpackChunkName: "error" */ "@/views/pages/error/Error-1"),
                },
            ],
        },
        {
            path: "/",
            component: () => import(/* webpackChunkName: "auth" */ "@/views/pages/auth/Auth"),
            children: [
                {
                    name: "login",
                    path: "/login",
                    component: () => import(/* webpackChunkName: "auth" */ "@/views/pages/auth/Login"),
                    meta: { requiresVisitor: true },
                },
                {
                    name: "forgot-password",
                    path: "/forgot-password",
                    component: () =>
                        import(/* webpackChunkName: "auth" */ "@/views/pages/auth/ForgotPassword"),
                    meta: { requiresVisitor: true },
                },
                {
                    name: "register",
                    path: "/register",
                    component: () => import(/* webpackChunkName: "auth" */ "@/views/pages/auth/Register"),
                },
                {
                    name: "register-complete",
                    path: "/register-complete/:email",
                    component: () =>
                        import(/* webpackChunkName: "auth" */ "@/views/pages/auth/RegisterComplete"),
                    props: true,
                },
                {
                    name: "oauth-callback",
                    path: "oauth/callback/:provider",
                    props: true,
                    component: () =>
                        import(/* webpackChunkName: "auth" */ "@/views/pages/auth/SocialLoginCallback"),
                },
                {
                    name: "oauth",
                    path: "oauth/:provider/:token?",
                    props: true,
                    component: () =>
                        import(/* webpackChunkName: "auth" */ "@/views/pages/auth/SocialAuth.vue"),
                },
                {
                    name: "mail-confirm",
                    path: "mail-confirm/:token",
                    component: () => import(/* webpackChunkName: "auth" */ "@/views/pages/auth/ConfirmMail"),
                },
                {
                    name: "email-link",
                    path: "email-link/:token",
                    component: () =>
                        import(/* webpackChunkName: "auth" */ "@/views/pages/auth/FromEmailLinkActivator"),
                },
                {
                    name: "change-account",
                    path: "change-account/:token/:accountId",
                    component: () =>
                        import(/* webpackChunkName: "auth" */ "@/views/pages/auth/ChangeAccount"),
                },
            ],
        },
        {
            path: "*",
            redirect: "/404",
        },
        {
            // the 404 route, when none of the above matches
            path: "/404",
            name: "404",
            component: () => import(/* webpackChunkName: "error" */ "@/views/pages/error/Error-1"),
        },
    ],
});

router.beforeEach(async (to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        let authenticated = false;
        try {
            authenticated = await store.dispatch(VERIFY_AUTH);
        } catch (error) {
            console.error("verify auth", error);
        }
        if (authenticated) {
            const currentProjects = store.state.auth.currentProjects;
            let isSetActiveAccount = false;
            if (BASE_DOMAIN !== location.host) {
                if (currentProjects) {
                    const domain = location.host.replace(`.${BASE_DOMAIN}`, "");
                    currentProjects.forEach((project) => {
                        if (project.domain === domain) {
                            console.log("SET_ACTIVE_ACCOUNT commit");
                            store.commit(SET_ACTIVE_ACCOUNT, project.id);
                            isSetActiveAccount = true;
                        }
                    });
                }
            } else {
                if (currentProjects.length === 1 && !BASE_DOMAIN.startsWith("localhost")) {
                    // if user has only one project-account redirect him to project domain
                    const authToken = localStorage.getItem(AUTH_TOKEN_KEY);
                    const project = currentProjects[0];
                    if (authToken && project.domain) {
                        localStorage.removeItem(AUTH_TOKEN_KEY);
                        location.href = project.domain
                            ? `${HTTP_PROTOCOL}://${project.domain}.${BASE_DOMAIN}/change-account/${authToken}/${project.id}`
                            : `${BASE_HOST}/change-account/${authToken}/${project.id}`;
                    }
                } else {
                    isSetActiveAccount = store.getters.idAccount;
                }
            }
            if (
                to.matched.some((record) => record.meta.requiresAuth) &&
                to.name !== "tariff" &&
                to.name !== "me-profile" &&
                to.name !== "me-notification-settings" &&
                to.name !== "me-change-password" &&
                !isSetActiveAccount
            ) {
                store.state.config.config.aside.self.display = false;
                if (!store.getters.idAccount && to.name !== "accounts") {
                    next({ name: "accounts" });
                    return;
                }
            }
            next();
        } else {
            localStorage.setItem(REDIRECT_URL_KEY, to.path);
            next({ name: "login" });
        }
    } else if (to.matched.some((record) => record.meta.requiresVisitor)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        const authenticated = await store.dispatch(VERIFY_AUTH);
        if (authenticated) {
            next({ name: "dashboard" });
        } else {
            next();
        }
    } else {
        next(); // make sure to always call next()!
    }
});

export default router;
